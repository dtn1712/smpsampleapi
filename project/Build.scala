import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "Airisumi"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    javaCore,
    javaEbean,
    cache,
    filters,
    "org.json" % "org.json" % "chargebee-1.0",
    "commons-io" % "commons-io" % "1.3.2",
    "redis.clients" % "jedis" % "2.1.0",
    "com.amazonaws" % "aws-java-sdk" % "1.3.11",
    "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
    "net.vz.mongodb.jackson" %% "play-mongo-jackson-mapper" % "1.1.0",
    "org.springframework" % "spring-context" % "4.0.3.RELEASE",
    "com.twilio.sdk" % "twilio-java-sdk" % "3.4.2",
    "com.typesafe" %% "play-plugins-mailer" % "2.1-RC2"
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here 


  )

}
