'use strict';

/* Services */

var octopusServices = angular.module('octopusServices', ['ngResource']);

octopusServices.factory('OctopusModel', ['$resource',
  function($resource){
    return $resource('/api/models/:id', {}, {
      query: {method:'GET', isArray:true}
    });
  }]);