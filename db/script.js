/*
 * Author: Sari Haj Hussein
 */

use airisumi

db.dropDatabase()

db.createCollection('user')

db.createCollection('comment')

db.createCollection('comparison')

db.createCollection('item')

db.createCollection('notification')

db.createCollection('photo')

db.createCollection('setting')

db.createCollection('vote')

