'use strict';

/* App Module */

var octopusApp = angular.module('octopusApp', [
  'octopusControllers',
  'octopusServices'
]);

octopusApp.config(['$routeProvider','$locationProvider',
  function($routeProvider,$locationProvider) {
	
	$locationProvider.html5Mode(true);
	
	$routeProvider.when("/", {
		templateUrl: "/assets/template/main.html",
		controller: "ListModelCtrl"
	}).when("/model/run/:id", {
		templateUrl: "/assets/template/main.html",
		controller: "RunModelCtrl"
    }).otherwise({
		redirectTo: "/"
	});
  }]);