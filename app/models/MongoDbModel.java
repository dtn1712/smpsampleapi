package models;

import java.util.List;
import java.util.Map;

/**
 * Created by dtn29 on 4/20/14.
 */
public abstract class MongoDbModel {

    public void expandRelatedFields(List<String> expandFields) {}

    public Map<String,Object> getFields() {return null; }

    public void buildFields() {}

    public void buildIndex() {}

    public void applySecurity() {}
}
