package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.BasicDBObject;
import helpers.GlobalHelper;
import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.JacksonDBCollection;
import net.vz.mongodb.jackson.ObjectId;
import net.vz.mongodb.jackson.WriteResult;
import play.Logger;
import play.modules.mongodb.jackson.MongoDB;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemAttribute extends MongoDbModel {

	@Id
	@ObjectId
	private String id;
	
	private String attributeName;
	private String item1Value;
	private String item2Value;
	private String comparisonId;

    private Map<String,Object> expandFields;

    @JsonIgnore
    private Map<String,Object> fields;

    public ItemAttribute(){
        this.expandFields = new HashMap<String,Object>();
        this.fields = new HashMap<String,Object>();
    }
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getAttributeName() {
		return attributeName;
	}
	
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	
	public String getItem1Value() {
		return item1Value;
	}
	
	public void setItem1Value(String item1Value) {
		this.item1Value = item1Value;
	}
	
	public String getItem2Value() {
		return item2Value;
	}
	
	public void setItem2Value(String item2Value) {
		this.item2Value = item2Value;
	}
	
	public String getComparisonId() {
		return comparisonId;
	}
	
	public void setComparisonId(String comparisonId) {
		this.comparisonId = comparisonId;
	}

    public Map<String, Object> getExpandFields() {
        return expandFields;
    }

    public void setExpandFields(Map<String, Object> expandFields) {
        this.expandFields = expandFields;
    }

    @Override
    public Map<String, Object> getFields() { return fields; }

    private static JacksonDBCollection<ItemAttribute, String> collection = MongoDB.getCollection("itemAttribute", ItemAttribute.class, String.class);

    public static JacksonDBCollection<ItemAttribute, String> getCollection() {
        return collection;
    }

    public static ItemAttribute getById(String id) {
        try {
            return collection.findOneById(id);
        } catch (Exception e) {
            Logger.error("Fail to get item attribute by id " + id, e);
        }
        return null;
    }

    public static void create(ItemAttribute itemAttribute) {
        try {
            if (itemAttribute == null) throw new NullPointerException();
            WriteResult<ItemAttribute,String> result = collection.insert(itemAttribute);
            itemAttribute.setId(result.getSavedId());
        } catch (Exception e) {
            Logger.error("Fail to create new item attribute",e);
        }
    }

    public static void bulkCreate(List<ItemAttribute> itemAttributes) {
        try {
            if (itemAttributes == null) throw new NullPointerException();
            WriteResult<ItemAttribute, String> result = collection.insert(itemAttributes);
            Object[] ids = result.getSavedIds();
            for (int i = 0; i < ids.length; i++) {
                itemAttributes.get(i).setId((String)ids[i]);
            }
        } catch (Exception e) {
            Logger.error("Fail to bulk create the item attribute",e);
        }
    }

    public static ItemAttribute update(ItemAttribute itemAttribute) {
        try {
            if (itemAttribute == null) throw new NullPointerException();
            WriteResult<ItemAttribute,String> result = collection.save(itemAttribute);
            return result.getSavedObject();
        } catch (Exception e) {
            Logger.error("Fail to update item attribute " + itemAttribute.getId(),e);
        }
        return null;
    }

    public static void delete(String id) {
        try {
            ItemAttribute itemAttribute = collection.findOneById(id);
            if (itemAttribute != null) {
                collection.remove(itemAttribute);
            }
        } catch (Exception e) {
            Logger.error("Fail to delete item attribute " + id, e);
        }

    }

    public static void bulkDelete(Map<String,String> queryCriteria) {
        if (queryCriteria == null || queryCriteria.size() == 0) return;
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
        for (Map.Entry<String, String> entry : queryCriteria.entrySet()) {
            obj.add(new BasicDBObject(entry.getKey(),new BasicDBObject("$regex",entry.getValue()).append("$options", "i")));
        }
        query.put("$and", obj);
        collection.remove(query);
    }

    public static ItemAttribute mapJsonToObject(JsonNode json, ItemAttribute itemAttribute) {
        try {
            if (json == null) throw new NullPointerException();
            if (itemAttribute == null) {
                itemAttribute = new ItemAttribute();
                itemAttribute.setAttributeName(json.findPath("attributeName").asText());
                itemAttribute.setComparisonId(json.findPath("comparisonId").asText());
                itemAttribute.setItem1Value(json.findPath("item1Value").asText());
                itemAttribute.setItem2Value(json.findPath("item2Value").asText());
            } else {
                if (json.findPath("attributeName").isTextual())  itemAttribute.setAttributeName(json.findPath("attributeName").asText());
                if (json.findPath("comparisonId").isTextual()) itemAttribute.setComparisonId(json.findPath("comparisonId").asText());
                if (json.findPath("item1Value").isDouble()) itemAttribute.setItem1Value(json.findPath("item1Value").asText());
                if (json.findPath("item2Value").isTextual()) itemAttribute.setItem2Value(json.findPath("item2Value").asText());
            }
            return itemAttribute;
        } catch (Exception e) {
            Logger.error("Fail to map json data to item attribute object",e);
        }
        return null;
    }

    @Override
    public void expandRelatedFields(List<String> expandFields){
        if (expandFields == null) return;
        if (this.expandFields == null) {
            this.expandFields = new HashMap<String,Object>();
        }
        for (String field : expandFields) {
            if (field.equals("comparison")) {
                this.expandFields.put("comparison", GlobalHelper.chooseMessageIfObjectNull(Comparison.getById(comparisonId), "Comparison Id " + comparisonId + " is invalid"));
            }
        }

    }

    @Override
    public void buildFields() {
        if (fields == null) {
            fields = new HashMap<String,Object>();
        }
        if (fields.containsKey("id") == false) {
            fields.put("id",id);
            fields.put("attributeName",attributeName);
            fields.put("comparisonId",comparisonId);
            fields.put("item1Value",item1Value);
            fields.put("item2Value",item2Value);
            fields.put("expand",expandFields);
        }
    }

    @Override
    public void buildIndex() {

    }
	
}
