package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;

import com.mongodb.BasicDBObject;
import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.JacksonDBCollection;
import net.vz.mongodb.jackson.ObjectId;
import net.vz.mongodb.jackson.WriteResult;
import play.Logger;
import play.modules.mongodb.jackson.MongoDB;

import java.util.*;

public class Setting extends MongoDbModel{
	
	private static enum NotificationOptions { OFF, FROM_FOLLOWING_PEOPLE, FROM_EVERYONE }

	@Id
	@ObjectId
	private String id;
	
	private String userId;
	private boolean isShareFacebook;
	private boolean isAllowFollowing;
    private String likeNotification;
    private String commentNotification;

    @JsonIgnore
    private Map<String,Object> fields;

	public Setting() {
		this.isShareFacebook = true;
		this.isAllowFollowing = true;
        this.likeNotification = NotificationOptions.FROM_EVERYONE.toString();
        this.commentNotification = NotificationOptions.FROM_EVERYONE.toString();
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public boolean isShareFacebook() {
		return isShareFacebook;
	}
	
	public void setShareFacebook(boolean isShareFacebook) {
		this.isShareFacebook = isShareFacebook;
	}
	
	public String getLikeNotification() {
		return likeNotification;
	}
	
	public void setLikeNotification(String likeNotification) {
		this.likeNotification = likeNotification;
	}
	
	public String getCommentNotification() {
		return commentNotification;
	}
	
	public void setCommentNotification(String commentNotification) {
		this.commentNotification = commentNotification;
	}
	
	public boolean isAllowFollowing() {
		return isAllowFollowing;
	}
	
	public void setAllowFollowing(boolean isAllowFollowing) {
		this.isAllowFollowing = isAllowFollowing;
	}

    @Override
    public Map<String, Object> getFields() { return fields; }

    private static JacksonDBCollection<Setting, String> collection = MongoDB.getCollection("setting", Setting.class, String.class);

    public static JacksonDBCollection<Setting, String> getCollection() {
        return collection;
    }

    public static Setting getById(String id) {
        try {
            return collection.findOneById(id);
        } catch (Exception e) {
            Logger.error("Fail to get setting by id " + id, e);
        }
        return null;
    }

    public static void create(Setting setting) {
        try {
            if (setting == null) throw new NullPointerException();
            WriteResult<Setting,String> result = collection.insert(setting);
            setting.setId(result.getSavedId());
        } catch (Exception e) {
            Logger.error("Fail to create new setting",e);
        }
    }

    public static Setting update(Setting setting) {
        try {
            if (setting == null) throw new NullPointerException();
            WriteResult<Setting,String> result = collection.save(setting);
            return result.getSavedObject();
        } catch (Exception e) {
            Logger.error("Fail to update setting " + setting.getId(),e);
        }
        return null;
    }

    public static void delete(String id) {
        try {
            Setting setting = collection.findOneById(id);
            if (setting != null) {
                collection.remove(setting);
            }
        } catch (Exception e) {
            Logger.error("Fail to delete setting " + id,e);
        }

    }

    public static void bulkDelete(Map<String,String> queryCriteria) {
        if (queryCriteria == null || queryCriteria.size() == 0) return;
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
        for (Map.Entry<String, String> entry : queryCriteria.entrySet()) {
            obj.add(new BasicDBObject(entry.getKey(),new BasicDBObject("$regex",entry.getValue()).append("$options", "i")));
        }
        query.put("$and", obj);
        collection.remove(query);
    }

    public static Setting mapJsonToObject(JsonNode json, Setting setting) {
        try {
            if (json == null) throw new NullPointerException();
            if (setting == null) {
                setting = new Setting();
            } else {
                if (json.findPath("isShareFacebook").isBoolean()) setting.setShareFacebook(json.findPath("isShareFacebook").asBoolean());
                if (json.findPath("isAllowFollowing").isBoolean()) setting.setAllowFollowing(json.findPath("isAllowFollowing").asBoolean());
                JsonNode likeNotification = json.findPath("likeNotification");
                if (likeNotification.isTextual() || isValidNotificationOption(likeNotification.asText())) {
                    setting.setLikeNotification(likeNotification.asText());
                }
                JsonNode commentNotification = json.findPath("commentNotification");
                if (commentNotification.isTextual() || isValidNotificationOption(commentNotification.asText())) {
                    setting.setCommentNotification(commentNotification.asText());
                }
            }
            return setting;
        } catch (Exception e) {
            Logger.error("Fail to map json data to user object",e);
        }
        return null;
    }

    @Override
    public void buildFields() {
        if (fields == null) {
            fields = new HashMap<String,Object>();
        }
        if (fields.containsKey("id") == false) {
            fields.put("id",id);
            fields.put("isShareFacebook",isShareFacebook);
            fields.put("isAllowFollowing",isAllowFollowing);
            fields.put("likeNotification",likeNotification);
            fields.put("commentNotification",commentNotification);
        }
    }

    @Override
    public void buildIndex() {

    }

    private static boolean isValidNotificationOption(String notificationOption) {
        if (notificationOption == null) return false;
        if (notificationOption.equals(NotificationOptions.OFF.toString()) ||
                notificationOption.equals(NotificationOptions.FROM_FOLLOWING_PEOPLE.toString()) ||
                notificationOption.equals(NotificationOptions.FROM_FOLLOWING_PEOPLE.toString())) {
            return true;
        }
        return false;
    }
}
