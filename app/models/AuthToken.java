package models;

import com.mongodb.BasicDBObject;
import net.vz.mongodb.jackson.*;
import play.Logger;
import play.modules.mongodb.jackson.MongoDB;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dtn29 on 4/13/14.
 */
public class AuthToken extends MongoDbModel {

    @Id
    @ObjectId
    private String id;

    private String userId;
    private String facebookId;
    private String authToken;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    private static JacksonDBCollection<AuthToken, String> collection = MongoDB.getCollection("authToken", AuthToken.class, String.class);

    public static JacksonDBCollection<AuthToken, String> getCollection() {
        return collection;
    }

    public static AuthToken getById(String id) {
        try {
            return collection.findOneById(id);
        } catch (Exception e) {
            Logger.error("Fail to get auth token by id " + id, e);
        }
        return null;
    }

    public static AuthToken getOneByUniqueField(String field, String data) {
        DBCursor<AuthToken> cursor = collection.find(DBQuery.is(field, data));
        if (cursor.hasNext()) {
            return cursor.next();
        }
        return null;
    }

    public static void create(AuthToken authToken) {
        try {
            if (authToken == null) throw new NullPointerException();
            WriteResult<AuthToken,String> result = collection.insert(authToken);
            authToken.setId(result.getSavedId());
        } catch (Exception e) {
            Logger.error("Fail to create new auth token",e);
        }
    }

    public static AuthToken update(AuthToken authToken) {
        try {
            if (authToken == null) throw new NullPointerException();
            WriteResult<AuthToken,String> result = collection.save(authToken);
            return result.getSavedObject();
        } catch (Exception e) {
            Logger.error("Fail to update auth token " + authToken.getId(),e);
        }
        return null;
    }

    public static void delete(String id) {
        try {
            AuthToken authToken = collection.findOneById(id);
            if (authToken != null) {
                collection.remove(authToken);
            }
        } catch (Exception e) {
            Logger.error("Fail to delete auth token " + id,e);
        }

    }

    public static void bulkDelete(Map<String,String> queryCriteria) {
        if (queryCriteria == null || queryCriteria.size() == 0) return;
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
        for (Map.Entry<String, String> entry : queryCriteria.entrySet()) {
            obj.add(new BasicDBObject(entry.getKey(),new BasicDBObject("$regex",entry.getValue()).append("$options", "i")));
        }
        query.put("$and", obj);
        collection.remove(query);
    }

    @Override
    public void buildIndex() {

    }

}
