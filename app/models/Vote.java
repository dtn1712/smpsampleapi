package models;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.BasicDBObject;
import helpers.GlobalHelper;
import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.JacksonDBCollection;
import net.vz.mongodb.jackson.ObjectId;
import net.vz.mongodb.jackson.WriteResult;
import play.Logger;
import play.modules.mongodb.jackson.MongoDB;

public class Vote extends MongoDbModel {

	@Id
	@ObjectId
	private String id;
	
	private String createUserId;
	private String itemId;
	private String comparisonId;
	private Date createTime;

    private Map<String,Object> expandFields;

    @JsonIgnore
    private Map<String,Object> fields;

    public Vote() {
        this.createTime = new Date();
        this.expandFields = new HashMap<String,Object>();
        this.fields = new HashMap<String,Object>();
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getComparisonId() {
		return comparisonId;
	}

	public void setComparisonId(String comparisonId) {
		this.comparisonId = comparisonId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

    public Map<String, Object> getExpandFields() {
        return expandFields;
    }

    public void setExpandFields(Map<String, Object> expandFields) {
        this.expandFields = expandFields;
    }

    @Override
    public Map<String, Object> getFields() {return fields;}

    private static JacksonDBCollection<Vote, String> collection = MongoDB.getCollection("vote", Vote.class, String.class);

    public static JacksonDBCollection<Vote, String> getCollection() {
        return collection;
    }

    public static Vote getById(String id) {
        try {
            return collection.findOneById(id);
        } catch (Exception e) {
            Logger.error("Fail to get vote by id " + id, e);
        }
        return null;
    }

    public static void create(Vote vote) {
        try {
            if (vote == null) throw new NullPointerException();
            WriteResult<Vote,String> result = collection.insert(vote);
            vote.setId(result.getSavedId());
        } catch (Exception e) {
            Logger.error("Fail to create new vote",e);
        }
    }

    public static Vote update(Vote vote) {
        try {
            if (vote == null) throw new NullPointerException();
            WriteResult<Vote,String> result = collection.save(vote);
            System.out.println("work");
            return result.getSavedObject();
        } catch (Exception e) {
            Logger.error("Fail to update vote " + vote.getId(),e);
        }
        return null;
    }

    public static void delete(String id) {
        try {
            Vote vote = collection.findOneById(id);
            if (vote != null) {
                collection.remove(vote);
            }
        } catch (Exception e) {
            Logger.error("Fail to delete vote " + id,e);
        }

    }

    public static void bulkDelete(Map<String,String> queryCriteria) {
        if (queryCriteria == null || queryCriteria.size() == 0) return;
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
        for (Map.Entry<String, String> entry : queryCriteria.entrySet()) {
            obj.add(new BasicDBObject(entry.getKey(),new BasicDBObject("$regex",entry.getValue()).append("$options", "i")));
        }
        query.put("$and", obj);
        collection.remove(query);
    }

    public static Vote mapJsonToObject(JsonNode json, Vote vote) {
        try {
            if (json == null) throw new NullPointerException();
            if (vote == null) {
                vote = new Vote();
                vote.setComparisonId(json.findPath("comparisonId").asText());
                vote.setItemId(json.findPath("itemId").asText());
                vote.setCreateUserId(json.findPath("createUserId").asText());
            } else {
                if (json.findPath("comparisonId").isTextual())  vote.setComparisonId(json.findPath("comparisonId").asText());
                if (json.findPath("itemId").isTextual()) vote.setItemId(json.findPath("itemId").asText());
                if (json.findPath("createUserId").isTextual()) vote.setCreateUserId(json.findPath("createUserId").asText());
            }
            return vote;
        } catch (Exception e) {
            Logger.error("Fail to map json data to user object",e);
        }
        return null;
    }

    @Override
    public void expandRelatedFields(List<String> expandFields){
        if (expandFields == null) return;
        if (this.expandFields == null) {
            this.expandFields = new HashMap<String,Object>();
        }
        for (String field : expandFields) {
            if (field.equals("comparison")) {
                this.expandFields.put("comparison", GlobalHelper.chooseMessageIfObjectNull(Comparison.getById(comparisonId), "Comparison Id " + comparisonId + " is invalid"));
            } else if (field.equals("item")) {
                this.expandFields.put("item",GlobalHelper.chooseMessageIfObjectNull(Item.getById(itemId),"Item Id " + itemId + " is in valid"));
            } else if (field.equals("createUser")) {
                User user = User.getById(createUserId);
                if (user == null) {
                    this.expandFields.put("createUser","User Id " + createUserId + " is invalid");
                } else {
                    user.applySecurity();
                    this.expandFields.put("createUser",user.getFields());
                }
            }
        }
    }

    @Override
    public void buildFields() {
        if (fields == null) {
            fields = new HashMap<String,Object>();
        }
        if (fields.containsKey("id") == false) {
            fields.put("id",id);
            fields.put("createUserId",createUserId);
            fields.put("itemId",itemId);
            fields.put("comparisonId",comparisonId);
            fields.put("createTime",createTime);
            fields.put("expand",expandFields);
        }
    }

    @Override
    public void buildIndex() {

    }
}
