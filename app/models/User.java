package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.BasicDBObject;
import net.vz.mongodb.jackson.*;
import play.Logger;
import play.modules.mongodb.jackson.MongoDB;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User extends MongoDbModel {

    private static final String[] restrictedField = new String[] { "email", "setting" };

	@Id
	@ObjectId
	private String id;
	
	private String facebookId;
	private String firstName;
	private String lastName;
	private String email;
	private String avatar;
	private String gender;
	private String bio;
	private Setting setting;

    private Map<String,String> comparisonPosts;
	
	private Map<String,String> followers;
	private Map<String,String> following;

    @JsonIgnore
    private Map<String,Object> fields;

    public User() {
        this.comparisonPosts = new HashMap<String,String>();
        this.followers = new HashMap<String,String>();
        this.following = new HashMap<String,String>();
    }
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

    public Map<String, String> getComparisonPosts() {
        return comparisonPosts;
    }

    public void setComparisonPosts(Map<String, String> comparisonPosts) {
        this.comparisonPosts = comparisonPosts;
    }

    public void addComparisonPost(Comparison comparison) {
        if (comparison == null) return;
        if (this.comparisonPosts == null) {
            this.comparisonPosts = new HashMap<String,String>();
        }
        this.followers.put(comparison.getId(), comparison.getCaption());
    }

    public void removeComparisonPost(Comparison comparison) {
        if (comparison == null) return;
        if (this.comparisonPosts == null) {
            this.comparisonPosts = new HashMap<String,String>();
        }
        this.comparisonPosts.remove(comparison.getId());
    }

    public Map<String,String> getFollowers() {
		return followers;
	}

	public void setFollowers(Map<String,String> followers) {
		this.followers = followers;
	}
	
	public void addFollowerUser(User user) {
        if (user == null) return;
		if (this.followers == null) {
			this.followers = new HashMap<String,String>();
		}
		this.followers.put(user.getId(), user.getFirstName() + " " + user.getLastName());
	}
	
	public void removeFollowerUser(User user) {
        if (user == null) return;
		if (this.followers == null) {
			this.followers = new HashMap<String,String>();
			return;
		}
		this.followers.remove(user.getId());
	}

	public Map<String,String> getFollowing() {
		return following;
	}

	public void setFollowing(Map<String,String> following) {
		this.following = following;
	}
	
	public void addFollowingUser(User user) {
        if (user == null) return;
		if (this.following == null) {
			this.following = new HashMap<String,String>();
		}
		this.following.put(user.getId(), user.getFirstName() + " " + user.getLastName());
	}
	
	public void removeFollowingUser(User user) {
        if (user == null) return;
		if (this.following == null) {
			this.following = new HashMap<String,String>();
			return;
		}
		this.following.remove(user.getId());
	}

	public Setting getSetting() {
		return setting;
	}

	public void setSetting(Setting setting) {
		this.setting = setting;
	}

    @Override
    public Map<String, Object> getFields() { return fields; }

	private static JacksonDBCollection<User, String> collection = MongoDB.getCollection("user", User.class, String.class);

    public static JacksonDBCollection<User, String> getCollection() {
        return collection;
    }

	public static User getByFacebookId(String facebookId) {
        if (facebookId == null) return null;
		DBCursor<User> cursor = collection.find(DBQuery.is("facebookId", facebookId));
		if (cursor.hasNext()) {
			return cursor.next();
		}
		return null;
	}
	
	public static User getById(String id) {
		try {
			return collection.findOneById(id);
		} catch (Exception e) {
			Logger.error("Fail to get user by id " + id,e);
		}
		return null;
	}
	  
	public static void create(User user) {
		try {
			if (user == null) throw new NullPointerException();
			WriteResult<User,String> result = collection.insert(user);
			user.setId(result.getSavedId());
		} catch (Exception e) {
			Logger.error("Fail to create new user",e);
		}
	}
	
	public static User update(User user) {
		try {
			if (user == null) throw new NullPointerException();
			WriteResult<User,String> result = collection.save(user);
			return result.getSavedObject();
		} catch (Exception e) {
			Logger.error("Fail to update user " + user.getId(),e);
		}
		return null;
	}

	public static void delete(String id) {
        try {
            User user = collection.findOneById(id);
            if (user != null) {
                collection.remove(user);
            }
        } catch (Exception e) {
            Logger.error("Fail to delete user " + id, e);
        }

	}

    public static void bulkDelete(Map<String,String> queryCriteria) {
        if (queryCriteria == null || queryCriteria.size() == 0) return;
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
        for (Map.Entry<String, String> entry : queryCriteria.entrySet()) {
            obj.add(new BasicDBObject(entry.getKey(),new BasicDBObject("$regex",entry.getValue()).append("$options", "i")));
        }
        query.put("$and", obj);
        collection.remove(query);
    }
	
	public static User mapJsonToObject(JsonNode json, User user) {
		try {
			if (json == null) throw new NullPointerException();
			if (user == null) {
				user = new User();
				user.setEmail(json.findPath("email").asText());
				user.setFirstName(json.findPath("firstName").asText());
				user.setLastName(json.findPath("lastName").asText());
				user.setFacebookId(json.findPath("facebookId").asText());
				user.setBio(json.findPath("bio").asText());
				user.setAvatar(json.findPath("avatar").asText());
				if (json.findPath("gender").textValue() == null) {
					user.setGender("male");
				} else {
					user.setGender(json.findPath("gender").asText());
				}				
			} else {
				if (json.findPath("email").isTextual()) user.setEmail(json.findPath("email").asText());
				if (json.findPath("firstName").isTextual()) user.setFirstName(json.findPath("firstName").asText());
				if (json.findPath("lastName").isTextual()) user.setLastName(json.findPath("lastName").asText());
				if (json.findPath("bio").isTextual()) user.setBio(json.findPath("bio").asText());
				if (json.findPath("avatar").isTextual()) user.setAvatar(json.findPath("avatar").asText());
				if (json.findPath("gender").isTextual()) user.setGender(json.findPath("gender").asText());
			}
			return user;
		} catch (Exception e) {
			Logger.error("Fail to map json data to user object",e);
		}
		return null;
	}

    @Override
    public void buildFields() {
        if (fields == null) {
            fields = new HashMap<String,Object>();
        }
        if (fields.containsKey("id") == false) {
            fields.put("id",id);
            fields.put("facebookId",facebookId);
            fields.put("email",email);
            fields.put("firstName",firstName);
            fields.put("lastName",lastName);
            fields.put("avatar",avatar);
            fields.put("gender",gender);
            fields.put("bio",bio);
            fields.put("setting",setting);
            fields.put("comparisonPosts",comparisonPosts);
            fields.put("followers",followers);
            fields.put("following",following);
            fields.put("authToken",null);
        }
    }

    @Override
    public void applySecurity() {
        this.buildFields();
        for (String field : restrictedField) {
            if (this.fields.containsKey(field)) {
                this.fields.remove(field);
            }
        }
    }

    @Override
    public void buildIndex() {

    }
}
