package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.BasicDBObject;
import helpers.GlobalHelper;
import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.JacksonDBCollection;
import net.vz.mongodb.jackson.ObjectId;
import net.vz.mongodb.jackson.WriteResult;
import play.Logger;
import play.modules.mongodb.jackson.MongoDB;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Item extends MongoDbModel{

	@Id
	@ObjectId
	private String id;
	
	private String name;
	private Double price;
	private String photoUrl;
	private String createUserId;
	private String comparisonId;

    private Map<String,Object> expandFields;

    @JsonIgnore
    private Map<String,Object> fields;

    public Item() {
        this.expandFields = new HashMap<String,Object>();
        this.fields = new HashMap<String,Object>();
    }
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getComparisonId() {
		return comparisonId;
	}

	public void setComparisonId(String comparisonId) {
		this.comparisonId = comparisonId;
	}

    public Map<String, Object> getExpandFields() {
        return expandFields;
    }

    public void setExpandFields(Map<String, Object> expandFields) {
        this.expandFields = expandFields;
    }

    @Override
    public Map<String, Object> getFields() { return fields; }

    private static JacksonDBCollection<Item, String> collection = MongoDB.getCollection("item", Item.class, String.class);

    public static JacksonDBCollection<Item, String> getCollection() {
        return collection;
    }

    public static Item getById(String id) {
        try {
            return collection.findOneById(id);
        } catch (Exception e) {
            Logger.error("Fail to get item by id " + id, e);
        }
        return null;
    }

    public static void create(Item item) {
        try {
            if (item == null) throw new NullPointerException();
            WriteResult<Item,String> result = collection.insert(item);
            item.setId(result.getSavedId());
        } catch (Exception e) {
            Logger.error("Fail to create new item",e);
        }
    }

    public static void bulkCreate(List<Item> items) {
        try {
            if (items == null) throw new NullPointerException();
            WriteResult<Item, String> result = collection.insert(items);
            Object[] ids = result.getSavedIds();
            for (int i = 0; i < ids.length; i++) {
                items.get(i).setId((String)ids[i]);
            }
        } catch (Exception e) {
            Logger.error("Fail to bulk create the item",e);
        }
    }

    public static Item update(Item item) {
        try {
            if (item == null) throw new NullPointerException();
            WriteResult<Item,String> result = collection.save(item);
            return result.getSavedObject();
        } catch (Exception e) {
            Logger.error("Fail to update item " + item.getId(),e);
        }
        return null;
    }

    public static void delete(String id) {
        try {
            Item item = collection.findOneById(id);
            if (item != null) {
                collection.remove(item);
            }
        } catch (Exception e) {
            Logger.error("Fail to delete item " + id,e);
        }

    }

    public static void bulkDelete(Map<String,String> queryCriteria) {
        if (queryCriteria == null || queryCriteria.size() == 0) return;
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
        for (Map.Entry<String, String> entry : queryCriteria.entrySet()) {
            obj.add(new BasicDBObject(entry.getKey(),new BasicDBObject("$regex",entry.getValue()).append("$options", "i")));
        }
        query.put("$and", obj);
        collection.remove(query);
    }

    public static Item mapJsonToObject(JsonNode json, Item item) {
        try {
            if (json == null) throw new NullPointerException();
            if (item == null) {
                item = new Item();
                item.setName(json.findPath("name").asText());
                item.setPrice(json.findPath("price").asDouble());
                item.setCreateUserId(json.findPath("createUserId").asText());
                item.setComparisonId(json.findPath("comparisonId").asText());
                item.setPhotoUrl(json.findPath("photoUrl").asText());
            } else {
                if (json.findPath("name").isTextual())  item.setName(json.findPath("name").asText());
                if (json.findPath("price").isDouble()) item.setPrice(json.findPath("price").asDouble());
                if (json.findPath("createUserId").isTextual()) item.setCreateUserId(json.findPath("createUserId").asText());
                if (json.findPath("comparisonId").isTextual()) item.setComparisonId(json.findPath("comparisonId").asText());
                if (json.findPath("photoUrl").isTextual()) item.setPhotoUrl(json.findPath("photoUrl").asText());
            }
            return item;
        } catch (Exception e) {
            Logger.error("Fail to map json data to item object",e);
        }
        return null;
    }

    @Override
    public void expandRelatedFields(List<String> expandFields){
        if (expandFields == null) return;
        if (this.expandFields == null) {
            this.expandFields = new HashMap<String,Object>();
        }
        for (String field : expandFields) {
            if (field.equals("comparison")) {
                this.expandFields.put("comparison", GlobalHelper.chooseMessageIfObjectNull(Comparison.getById(comparisonId), "Comparison Id " + comparisonId + " is invalid"));
            } else if (field.equals("createUser")) {
                User user = User.getById(createUserId);
                if (user == null) {
                    this.expandFields.put("createUser","User Id " + createUserId + " is invalid");
                } else {
                    user.applySecurity();
                    this.expandFields.put("createUser",user.getFields());
                }
            }
        }

    }

    @Override
    public void buildFields() {
        if (fields == null) {
            fields = new HashMap<String,Object>();
        }
        if (fields.containsKey("id") == false) {
            fields.put("id",id);
            fields.put("name",name);
            fields.put("price",price);
            fields.put("photoUrl",photoUrl);
            fields.put("createUserId",createUserId);
            fields.put("comparisonId",comparisonId);
            fields.put("expand",expandFields);
        }
    }

    @Override
    public void buildIndex() {

    }
}
