package models;

import com.mongodb.BasicDBObject;
import net.vz.mongodb.jackson.*;
import play.Logger;
import play.modules.mongodb.jackson.MongoDB;
import services.messaging.MessagingService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by dtn29 on 4/12/14.
 */
public class IPRequest extends MongoDbModel{

    @Id
    @ObjectId
    private String id;

    private String ipAddress;
    private String userAgent;
    private List<Date> requestTimes;

    public IPRequest() {}

    public IPRequest(String ipAddress, String userAgent, List<Date> requestTimes) {
        this.ipAddress = ipAddress;
        this.requestTimes = requestTimes;
        this.userAgent = userAgent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public List<Date> getRequestTimes() {
        return requestTimes;
    }

    public void setRequestTimes(List<Date> requestTimes) {
        this.requestTimes = requestTimes;
    }

    public void addRequestTime(Date requestTime) {
        if (this.requestTimes == null) {
            this.requestTimes = new ArrayList<Date>();
        }
        this.requestTimes.add(requestTime);
    }

    private static JacksonDBCollection<IPRequest, String> collection = MongoDB.getCollection("ipRequest", IPRequest.class, String.class);

    public static JacksonDBCollection<IPRequest, String> getCollection() {
        return collection;
    }

    public static IPRequest getById(String id) {
        try {
            return collection.findOneById(id);
        } catch (Exception e) {
            Logger.error("Fail to get IP Request by id " + id, e);
        }
        return null;
    }

    public static IPRequest getByIpAddress(String ipAddress) {
        DBCursor<IPRequest> cursor = collection.find(DBQuery.is("ipAddress", ipAddress));
        IPRequest ipRequest = null;
        try {
            if (cursor.hasNext()) {
                ipRequest = cursor.next();
            }

            /**
             * IP address should be unique, if there is more than one which should not be,
             * save the request time and delete it
             */
            while (cursor.hasNext()) {
                for (Date requestTime : cursor.next().getRequestTimes()) {
                    ipRequest.addRequestTime(requestTime);
                }
                delete(cursor.next().getId());
            }
        } catch (Exception e) {
            Logger.error("Cannot connect to the mongodb server. Please check the network",e);
        }
        return ipRequest;
    }

    public static void create(IPRequest ipRequest) {
        try {
            if (ipRequest == null) throw new NullPointerException();
            WriteResult<IPRequest,String> result = collection.insert(ipRequest);
            ipRequest.setId(result.getSavedId());
        } catch (Exception e) {
            Logger.error("Fail to create new IP Request",e);
        }
    }

    public static IPRequest update(IPRequest ipRequest) {
        try {
            if (ipRequest == null) throw new NullPointerException();
            WriteResult<IPRequest,String> result = collection.save(ipRequest);
            return result.getSavedObject();
        } catch (Exception e) {
            Logger.error("Fail to update IP Request " + ipRequest.getId(),e);
        }
        return null;
    }

    public static void delete(String id) {
        try {
            IPRequest ipRequest = collection.findOneById(id);
            if (ipRequest != null) {
                collection.remove(ipRequest);
            }
        } catch (Exception e) {
            Logger.error("Fail to delete IP Request " + id,e);
        }

    }

    public static void bulkDelete(Map<String,String> queryCriteria) {
        if (queryCriteria == null || queryCriteria.size() == 0) return;
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
        for (Map.Entry<String, String> entry : queryCriteria.entrySet()) {
            obj.add(new BasicDBObject(entry.getKey(),new BasicDBObject("$regex",entry.getValue()).append("$options", "i")));
        }
        query.put("$and", obj);
        collection.remove(query);
    }

    @Override
    public void buildIndex() {

    }
}
