package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.BasicDBObject;
import helpers.GlobalHelper;
import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.JacksonDBCollection;
import net.vz.mongodb.jackson.ObjectId;
import net.vz.mongodb.jackson.WriteResult;
import play.Logger;
import play.modules.mongodb.jackson.MongoDB;

import java.util.*;

public class Comparison extends MongoDbModel {

	@Id
	@ObjectId
	private String id;
	
	private String item1Id;
	private String item2Id;
	private String caption;
	private String createUserId;
	private Date createTime;
    private Date editTime;
	private boolean isAnonymousPosting;
	private boolean isShowPrice;

    private Map<String,Object> expandFields;

    @JsonIgnore
    private Map<String,Object> fields;
	
	public Comparison() {
		this.isAnonymousPosting = false;
		this.isShowPrice = true;
        this.createTime = new Date();
        this.expandFields = new HashMap<String,Object>();
        this.fields = new HashMap<String,Object>();
	}

	public boolean isAnonymousPosting() {
		return isAnonymousPosting;
	}

	public void setAnonymousPosting(boolean isAnonymousPosting) {
		this.isAnonymousPosting = isAnonymousPosting;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

    public String getItem1Id() {
        return item1Id;
    }

    public void setItem1Id(String item1Id) {
        this.item1Id = item1Id;
    }

    public String getItem2Id() {
        return item2Id;
    }

    public void setItem2Id(String item2Id) {
        this.item2Id = item2Id;
    }

    public String getCaption() {
		return caption;
	}
	
	public void setCaption(String caption) {
		this.caption = caption;
	}
	
	public String getCreateUserId() {
		return createUserId;
	}
	
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

    public Date getEditTime() {
        return editTime;
    }

    public void setEditTime(Date editTime) {
        this.editTime = editTime;
    }

    public boolean isShowPrice() {
		return isShowPrice;
	}

	public void setShowPrice(boolean isShowPrice) {
		this.isShowPrice = isShowPrice;
	}

    public Map<String, Object> getExpandFields() {
        return expandFields;
    }

    public void setExpandFields(Map<String, Object> expandFields) {
        this.expandFields = expandFields;
    }

    @Override
    public Map<String, Object> getFields() { return fields; }

    private static JacksonDBCollection<Comparison, String> collection = MongoDB.getCollection("comparison", Comparison.class, String.class);

    public static JacksonDBCollection<Comparison, String> getCollection() {
        return collection;
    }

    public static Comparison getById(String id) {
        try {
            return collection.findOneById(id);
        } catch (Exception e) {
            Logger.error("Fail to get comparison by id " + id, e);
        }
        return null;
    }

    public static void create(Comparison comparison) {
        try {
            if (comparison == null) throw new NullPointerException();
            WriteResult<Comparison,String> result = collection.insert(comparison);
            comparison.setId(result.getSavedId());
        } catch (Exception e) {
            Logger.error("Fail to create new comparison",e);
        }
    }

    public static Comparison update(Comparison comparison) {
        try {
            if (comparison == null) throw new NullPointerException();
            WriteResult<Comparison,String> result = collection.save(comparison);
            return result.getSavedObject();
        } catch (Exception e) {
            Logger.error("Fail to update comparison " + comparison.getId(),e);
        }
        return null;
    }

    public static void delete(String id) {
        try {
            Comparison comparison = collection.findOneById(id);
            if (comparison != null) {
                collection.remove(comparison);
            }
        } catch (Exception e) {
            Logger.error("Fail to delete comparison " + id, e);
        }

    }

    public static void bulkDelete(Map<String,String> queryCriteria) {
        if (queryCriteria == null || queryCriteria.size() == 0) return;
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
        for (Map.Entry<String, String> entry : queryCriteria.entrySet()) {
            obj.add(new BasicDBObject(entry.getKey(),new BasicDBObject("$regex",entry.getValue()).append("$options", "i")));
        }
        query.put("$and", obj);
        collection.remove(query);
    }

    public static Comparison mapJsonToObject(JsonNode json, Comparison comparison) {
        try {
            if (json == null) throw new NullPointerException();
            if (comparison == null) {
                comparison = new Comparison();
                comparison.setItem1Id(json.findPath("item1Id").asText());
                comparison.setItem2Id(json.findPath("item2Id").asText());
                comparison.setCaption(json.findPath("caption").asText());
                comparison.setCreateUserId(json.findPath("createUserId").asText());
                if (json.findPath("isAnonymousPosting").isBoolean()) comparison.setAnonymousPosting(json.findPath("isAnonymousPosting").asBoolean());
                if (json.findPath("isShowPrice").isBoolean()) comparison.setShowPrice(json.findPath("isShowPrice").asBoolean());
            } else {
                if (json.findPath("item1Id").isTextual())  comparison.setItem1Id(json.findPath("item1Id").asText());
                if (json.findPath("item2Id").isTextual()) comparison.setItem2Id(json.findPath("item2Id").asText());
                if (json.findPath("caption").isTextual()) comparison.setCaption(json.findPath("caption").asText());
                if (json.findPath("createUserId").isTextual()) comparison.setCreateUserId(json.findPath("createUserId").asText());
                if (json.findPath("isAnonymousPosting").isBoolean()) comparison.setAnonymousPosting(json.findPath("isAnonymousPosting").asBoolean());
                if (json.findPath("isShowPrice").isBoolean()) comparison.setShowPrice(json.findPath("isShowPrice").asBoolean());
                comparison.setEditTime(new Date());
            }
            return comparison;
        } catch (Exception e) {
            Logger.error("Fail to map json data to comparison object",e);
        }
        return null;
    }

    @Override
    public void expandRelatedFields(List<String> expandFields){
        System.out.println(expandFields);
        if (expandFields == null) return;
        if (this.expandFields == null) {
            this.expandFields = new HashMap<String,Object>();
        }
        for (String field : expandFields) {
            if (field.equals("item1")) {
                this.expandFields.put("item1",GlobalHelper.chooseMessageIfObjectNull(Item.getById(item1Id), "Item 1 Id " + item1Id + " is invalid"));
            } else if (field.equals("item2")) {
                this.expandFields.put("item2",GlobalHelper.chooseMessageIfObjectNull(Item.getById(item2Id), "Item 2 Id " + item2Id + " is invalid"));
            } else if (field.equals("createUser")) {
                User user = User.getById(createUserId);
                if (user == null) {
                    this.expandFields.put("createUser","User Id " + createUserId + " is invalid");
                } else {
                    user.applySecurity();
                    this.expandFields.put("createUser",user.getFields());
                }
            }
        }
    }

    @Override
    public void buildFields() {
        if (fields == null) {
            fields = new HashMap<String,Object>();
        }
        if (fields.containsKey("id") == false) {
            fields.put("id",id);
            fields.put("item1Id",item1Id);
            fields.put("item2Id",item2Id);
            fields.put("caption",caption);
            fields.put("createUserId",createUserId);
            fields.put("createTime",createTime);
            fields.put("isAnonymousPosting",isAnonymousPosting);
            fields.put("isShowPrice",isShowPrice);
            fields.put("expand",expandFields);
        }
    }

    @Override
    public void buildIndex() {

    }
}
