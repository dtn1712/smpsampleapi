package services.security;

import org.apache.http.HttpStatus;

import play.mvc.Results;
import play.mvc.SimpleResult;
import play.mvc.Http.Request;


public class SecurityException extends Exception {

	private Request request;
    private String message;
    private int httpStatus;
	
	public SecurityException(int httpStatus,String message,Request request) {
        super(message);
		this.request = request;
        this.message = message;
        this.httpStatus = httpStatus;
	}

    public String getIPRequest() {
        return request.remoteAddress();
    }

    public String getMessage() {
        return message;
    }

    public SimpleResult getExceptionResult() {
        if (httpStatus == HttpStatus.SC_UNAUTHORIZED) {
            return Results.unauthorized(message);
        } else {
            return Results.badRequest(message);
        }
    }

}
