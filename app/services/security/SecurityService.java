package services.security;

import play.Logger;
import play.Play;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.SimpleResult;
import services.security.impl.AuthTokenValidator;

public class SecurityService  extends Action.Simple{

	private RequestValidator[] validators = new RequestValidator[] {
            new AuthTokenValidator()
    };

    @Override
    public F.Promise<SimpleResult> call(Http.Context ctx) throws Throwable {
        try {
            IPService.logIpRequest(ctx.request());
            if (ctx.request().method().toUpperCase().equals("GET") == false) {
                for (RequestValidator validator : validators) {
                    validator.validate(ctx);
                }
            }
            return delegate.call(ctx);
        } catch (SecurityException e) {
            Logger.warn("Validate Request Fail from IP " + e.getIPRequest(),e);
            final SimpleResult exceptionResult = e.getExceptionResult();
            return F.Promise.promise(
                new F.Function0<SimpleResult>() {
                    @Override
                    public SimpleResult apply() throws Throwable {
                        return exceptionResult;
                    }
                }
            );
        }

    }

}
