package services.security;

import configs.Constants;
import models.IPRequest;
import play.mvc.Http.Request;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class IPService {
	
	public static void logIpRequest(Request request) {
		String userAgent = request.getHeader("User-Agent");
        if (userAgent == null) {
            userAgent = Constants.USER_AGENT_UNDEFINED;
        }
        String ipAddress = request.remoteAddress();
        Date now = new Date();
        IPRequest ipRequest = IPRequest.getByIpAddress(ipAddress);
        if (ipRequest != null) {
            ipRequest.addRequestTime(now);
            IPRequest.update(ipRequest);
        } else {
            List<Date> requestTimes = new ArrayList<Date>();
            requestTimes.add(now);
            ipRequest = new IPRequest(ipAddress,userAgent,requestTimes);
            IPRequest.create(ipRequest);
        }
    }
	
}
