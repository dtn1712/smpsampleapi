package services.security;

import play.mvc.Http;
import play.mvc.Http.Request;

public interface RequestValidator {

	public void validate(Http.Context ctx) throws SecurityException;
	
}
