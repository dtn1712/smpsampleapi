package services.messaging;

/**
 * Created by dtn29 on 4/22/14.
 */
public class MessagingException extends Exception {

    private String message;

    public MessagingException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
