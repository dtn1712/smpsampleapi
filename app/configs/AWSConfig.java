package configs;

public class AWSConfig {
	
	public static final String AWS_S3_BUCKET = "aws.s3.bucket";
	 
	public static final String AWS_ACCESS_KEY = "aws.access.key";
	    
	public static final String AWS_SECRET_KEY = "aws.secret.key";

}
