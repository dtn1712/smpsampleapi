package helpers;

import com.fasterxml.jackson.databind.JsonNode;
import configs.Constants;
import models.AuthToken;
import models.User;
import play.Play;
import play.mvc.Http;
import services.security.impl.AuthTokenValidator;

public class UserHelper {

	public static boolean isUserExist(String facebookId) {
		User user = User.getByFacebookId(facebookId);
		if (user == null) {
			return false;
		}
		return true;
	}

    public static boolean isFacebookIdValid(String facebookId) {
        JsonNode result = HttpHelper.callHTTP(Constants.FACEBOOK_GRAPH_URL + facebookId);
        if (result.get(Constants.ERROR_FIELD_KEY) == null) {
            return true;
        }
        return false;
    }

    public static boolean isAuthTokenValid(String authToken) {
        JsonNode result = HttpHelper.callHTTP(String.format(Constants.FACEBOOK_GRAPH_DEBUG_ACCESS_TOKEN_URL,authToken,authToken));
        if (result.get(Constants.ERROR_FIELD_KEY) == null) {
            return true;
        }
        return false;
    }

    public static boolean isNeedApplySecurity(Http.Context ctx, String id) {
        String currentLoginUserId = null;
        if (ctx.request().method().toUpperCase().equals("GET")) {
            AuthTokenValidator authTokenValidator = new AuthTokenValidator();
            AuthToken authToken = authTokenValidator.getHeaderAuthToken(ctx);
            if (authToken == null) return true;
            currentLoginUserId = authToken.getUserId();
        } else {
            currentLoginUserId = (String) ctx.args.get(Constants.CURRENT_LOGIN_USER);
        }
        if ( currentLoginUserId == null || currentLoginUserId.equals(id) == false) {
            return true;
        }
        return false;
    }
	
}
