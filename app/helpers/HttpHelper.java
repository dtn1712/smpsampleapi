package helpers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableMap;
import configs.Constants;
import models.MongoDbModel;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.api.libs.ws.Response;
import play.api.libs.ws.WS;
import play.libs.Json;
import play.mvc.Http;
import scala.collection.immutable.Stream;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by dtn29 on 4/12/14.
 */
public class HttpHelper {

    private static final Map<String, String> notFilterKeys = ImmutableMap.<String, String>builder()
            .put(Constants.SORT_KEYWORD, "")
            .put(Constants.LIMIT_FIELDS, "")
            .put(Constants.EXPAND_FIELDS, "")
            .put(Constants.PAGE_NUMBER, "")
            .put(Constants.NUM_RESULTS,"")
            .build();

    public static JsonNode callHTTP(String httpUrl) {
        Future<Response> future = WS.url(httpUrl).get();
        try {
            Response result = future.result(Duration.apply(30, TimeUnit.SECONDS), null);
            return Json.parse(result.json().toString());
        } catch (Exception e) {
            Logger.error("Call HTTP Exception", e);
        }
        return null;
    }

    public static List<String> buildLimitFieldsFromRequest(Http.Request request) {
        String fields = request.getQueryString(Constants.LIMIT_FIELDS);
        if (fields != null) {
            return Arrays.asList(fields.split(","));
        }
        return null;
    }

    public static List<String> buildExpandFieldsFromRequest(Http.Request request) {
        String fields = request.getQueryString(Constants.EXPAND_FIELDS);
        if (fields != null) {
            return Arrays.asList(fields.split(","));
        }
        return null;
    }

    public static Map<String, String> buildSortCriteriaFromRequest(Http.Request request) {
        String sort = request.getQueryString(Constants.SORT_KEYWORD);
        Map<String,String> results = new HashMap<String,String>();
        if (sort == null) {
            results.put(Constants.SORT_FIELD,null);
            results.put(Constants.SORT_TYPE,null);
        } else {
            if (sort.startsWith("-")) {
                results.put(Constants.SORT_FIELD,sort.substring(1));
                results.put(Constants.SORT_TYPE, Constants.DESCENDING_SORT);
            } else {
                results.put(Constants.SORT_FIELD,sort);
                results.put(Constants.SORT_TYPE,Constants.ASCENDING_SORT);
            }
        }
        return results;
    }

    public static Map<String,String> buildQueryCriteriaFromRequest(Http.Request request) throws Exception {
        Map<String, String[]> requestParameter = request.queryString();
        Map<String,String> result = new HashMap<String, String>();
        for (Map.Entry<String,String[]> entry : requestParameter.entrySet()) {
            if (notFilterKeys.containsKey(entry.getKey()) == false) {
                if (entry.getKey().contains("[") && entry.getKey().contains("]")) {
                    String operator = entry.getKey().substring(entry.getKey().indexOf("[")+1,entry.getKey().indexOf("]"));
                    if (Constants.COMPARISON_QUERY_OPERATORS.containsKey(operator) == false ) throw new Exception("The comparison query operator is invalid");
                    result.put(entry.getKey().substring(0,entry.getKey().indexOf("[")),Constants.COMPARISON_QUERY_OPERATORS.get(operator) + Constants.FIELD_SEPARATOR + entry.getValue()[0]);
                } else {
                    result.put(entry.getKey(),entry.getValue()[0]);
                }
            }
        }
        return result;
    }

    public static Map<String,Integer> buildPaginationFromRequest(Http.Request request) {
        Map<String,Integer> result = new HashMap<String,Integer>();
        String pageNum = request.getQueryString(Constants.PAGE_NUMBER);
        String numResults = request.getQueryString(Constants.NUM_RESULTS);
        if (numResults == null || StringUtils.isNumeric(numResults) == false){
            result.put(Constants.NUM_RESULTS,Constants.DEFAULT_TOTAL_RETURN);
        } else {
            result.put(Constants.NUM_RESULTS,Integer.parseInt(numResults));
        }
        if (pageNum == null || StringUtils.isNumeric(pageNum)) {
            result.put(Constants.PAGE_NUMBER,0);
        } else {
            result.put(Constants.PAGE_NUMBER,Integer.parseInt(pageNum));
        }
        return result;
    }

    public static ObjectNode buildDataResponse(int httpCode, Object obj) {
        ObjectNode result = Json.newObject();
        result.put(Constants.HTTP_STATUS,httpCode);
        if (obj instanceof JsonNode) {
            result.put(Constants.HTTP_RESULTS,(JsonNode) obj);
        } else {
            result.put(Constants.HTTP_RESULTS,Json.toJson(obj));
        }
        return result;
    }

    public static ObjectNode buildDataResponse(int httpCode, String message) {
        ObjectNode result = Json.newObject();
        result.put(Constants.HTTP_STATUS,httpCode);
        result.put(Constants.HTTP_NORMAL_MESSAGE,message);
        return result;
    }

    public static ObjectNode buildErrorFormatResponse(int httpCode, String message) {
        ObjectNode result = Json.newObject();
        result.put(Constants.HTTP_CODE,httpCode);
        result.put(Constants.HTTP_ERROR_MESSAGE,message);
        return result;
    }

    public static ObjectNode buildErrorFormatResponse(int httpCode, String message, String exceptionMessage) {
        ObjectNode result = Json.newObject();
        result.put(Constants.HTTP_CODE,httpCode);
        result.put(Constants.HTTP_ERROR_MESSAGE,message);
        result.put(Constants.HTTP_EXCEPTION_MESSAGE,exceptionMessage);
        return result;
    }

    public static boolean isJsonRequestContainValidFields(MongoDbModel testModel, JsonNode json) {
        testModel.buildFields();
        Map<String,Object> fields = testModel.getFields();
        Iterator<String> fieldNames = json.fieldNames();
        while (fieldNames.hasNext()) {
            String fieldName = fieldNames.next();
            if (fields.containsKey(fieldName) == false) {
                return false;
            }
        }
        return true;
    }
}
