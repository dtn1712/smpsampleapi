package helpers;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableMap;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoException;
import configs.Constants;
import models.MongoDbModel;
import net.vz.mongodb.jackson.DBCursor;
import net.vz.mongodb.jackson.JacksonDBCollection;
import play.libs.Json;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dtn29 on 4/12/14.
 */
public class MongoDbHelper {

    public static List<ObjectNode> queryMongoDb(JacksonDBCollection collection,Map<String,String> queryCriteria, String sortField,
                                                  String sortType, int pageNum, int numResults,
                                                  List<String> limitFields, List<String> expandFields) throws MongoException{
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
        if (queryCriteria != null) {
            for (Map.Entry<String, String> entry : queryCriteria.entrySet()) {
                if (entry.getValue().contains(Constants.FIELD_SEPARATOR)) {
                    String operator = entry.getValue().substring(0,entry.getValue().indexOf(Constants.FIELD_SEPARATOR));
                    String value = entry.getValue().substring(entry.getValue().indexOf(Constants.FIELD_SEPARATOR) + 2);
                    obj.add(new BasicDBObject(entry.getKey(),new BasicDBObject(operator,Double.parseDouble(value))));
                } else if (Constants.NUMERIC_COMPARE_FIELD_KEY.containsKey(entry.getKey())) {
                    obj.add(new BasicDBObject(entry.getKey(),Double.parseDouble(entry.getValue())));
                } else {
                    obj.add(new BasicDBObject(entry.getKey(),new BasicDBObject("$regex",entry.getValue()).append("$options", "i")));
                }
            }
            if (obj.size() > 0 ) query.put("$and", obj);
        }

        BasicDBObject limitFieldsQuery = new BasicDBObject();
        if (limitFields != null) {
            for (String field : limitFields) {
                System.out.println(field);
                limitFieldsQuery.put(field,1);
            }
        }

        BasicDBObject sortQuery = new BasicDBObject();
        if (sortField != null) {
            if (sortType.equals(Constants.ASCENDING_SORT)) {
                sortQuery.put(sortField,"1");
            } else {
                sortQuery.put(sortField,"-1");
            }
        }

        DBCursor cursor = null;
        if (pageNum > 0) {
            long totalItem = collection.count(query);
            if (pageNum * numResults > totalItem) {
                throw new MongoException("The page number exceed the available item");
            }
            DBCursor paginationCursor = collection.find(query, limitFieldsQuery).sort(sortQuery).limit(pageNum * numResults);
            MongoDbModel lastItem = null;
            while (paginationCursor.hasNext()) {
                lastItem = (MongoDbModel) cursor.next();
            }
            if (sortType.equals(Constants.ASCENDING_SORT)) {
                obj.add(new BasicDBObject(sortField,new BasicDBObject("$gt",lastItem.getFields().get(sortField))));
            } else {
                obj.add(new BasicDBObject(sortField,new BasicDBObject("$lt",lastItem.getFields().get(sortField))));
            }
            if (query.containsField("$and") == false) query.put("$and",obj);
            cursor = collection.find(query, limitFieldsQuery).sort(sortQuery).limit(numResults);
        } else {
            cursor = collection.find(query, limitFieldsQuery).sort(sortQuery).limit(Constants.DEFAULT_TOTAL_RETURN);
        }

        List<ObjectNode> result = new ArrayList<ObjectNode>();
        while (cursor.hasNext()){
            ObjectNode item = Json.newObject();
            MongoDbModel model = (MongoDbModel) cursor.next();
            model.applySecurity();
            if (expandFields != null) model.expandRelatedFields(expandFields);
            model.buildFields();
            Map<String,Object> allFields = model.getFields();
            Map<String,JsonNode> data = GlobalHelper.getLimitFieldsReturn(allFields,limitFields);
            item.putAll(data);
            result.add(item);
        }
        return result;
    }

}
