//package controllers;
//
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.node.ObjectNode;
//import configs.Constants;
//import helpers.GlobalHelper;
//import helpers.HttpHelper;
//import helpers.MongoDbHelper;
//import models.Photo;
//import models.User;
//import play.Logger;
//import play.libs.Json;
//import play.mvc.*;
//import services.security.SecurityService;
//
//import java.util.List;
//import java.util.Map;
//
//
//@With(SecurityService.class)
//public class Photos extends Controller{
//
//    public static Result list() {
//        try {
//            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
//            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
//            Map<String,String> sortCriteria = HttpHelper.buildSortCriteriaFromRequest(request());
//            Map<String,String> queryCriteria = HttpHelper.buildQueryCriteriaFromRequest(request());
//            Map<String,Integer> pagination = HttpHelper.buildPaginationFromRequest(request());
//            List<ObjectNode> photos = MongoDbHelper.queryMongoDb(Photo.getCollection(), queryCriteria,
//                    sortCriteria.get(Constants.SORT_FIELD), sortCriteria.get(Constants.SORT_TYPE),
//                    pagination.get(Constants.PAGE_NUMBER), pagination.get(Constants.NUM_RESULTS),
//                    limitFields, expandFields);
//            return ok(HttpHelper.buildDataResponse(200,photos));
//        } catch (Exception e) {
//            Logger.error("Cannot list the photos",e);
//            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Fail to execute the query",e.getMessage()));
//        }
//    }
//
//    @BodyParser.Of(BodyParser.Json.class)
//    public static Result create() {
//        JsonNode json = request().body().asJson();
//        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
//        try {
//            if (HttpHelper.isJsonRequestContainValidFields(new Photo(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
//            Photo newPhoto = Photo.mapJsonToObject(json,null);
//            User createUser = User.getById(newPhoto.getCreateUserId());
//            if (createUser == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The create user id " + newPhoto.getCreateUserId() + " is invalid"));
//            Photo.create(newPhoto);
//            return Results.created(HttpHelper.buildDataResponse(201,newPhoto));
//        } catch (Exception e) {
//            Logger.error("Create photo exception",e);
//            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot create photo",e.getMessage()));
//        }
//    }
//
//    public static Result get(String id) {
//        Photo photo = Photo.getById(id);
//        ObjectNode result = Json.newObject();
//        if (photo == null) {
//            return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the photo with id " + id));
//        } else {
//            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
//            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
//            if (expandFields != null) photo.expandRelatedFields(expandFields);
//            if (limitFields != null) {
//                photo.buildFields();
//                Map<String,Object> allFields = photo.getFields();
//                Map<String,JsonNode> data = GlobalHelper.getLimitFieldsReturn(allFields, limitFields);
//                result.putAll(data);
//                return ok(HttpHelper.buildDataResponse(200,result));
//            }
//            return ok(HttpHelper.buildDataResponse(200, photo));
//        }
//    }
//
//    @BodyParser.Of(BodyParser.Json.class)
//    public static Result update(String id) {
//        JsonNode json = request().body().asJson();
//        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
//        try {
//            Photo oldPhoto = Photo.getById(id);
//            if (oldPhoto == null) return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find photo with id " + id));
//            if (HttpHelper.isJsonRequestContainValidFields(new Photo(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
//            Photo newPhoto = Photo.mapJsonToObject(json, oldPhoto);
//            Photo updatedPhoto = Photo.update(newPhoto);
//            if (updatedPhoto == null) throw new Exception("Fail to update the photo. Please try again");
//            return ok(HttpHelper.buildDataResponse(200,updatedPhoto));
//        } catch (Exception e) {
//            Logger.error("Update photo exception",e);
//            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot update photo",e.getMessage()));
//        }
//    }
//
//    public static Result delete(String id) {
//        try {
//            Photo photo = Photo.getById(id);
//            if (photo == null) {
//                return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the photo to delete"));
//            }
//            Photo.delete(id);
//            return ok(HttpHelper.buildDataResponse(200,"Delete photo successfully"));
//        } catch (Exception e) {
//            Logger.error("Delete photo exception", e);
//            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot delete photo",e.getMessage()));
//        }
//    }
//
//}
