package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import configs.Constants;
import helpers.GlobalHelper;
import helpers.HttpHelper;
import helpers.MongoDbHelper;
import models.*;
import play.Logger;
import play.libs.Json;
import play.mvc.*;
import services.security.SecurityService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@With(SecurityService.class)
public class Items extends Controller {

    public static Result list() {
        try {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            Map<String,String> sortCriteria = HttpHelper.buildSortCriteriaFromRequest(request());
            Map<String,String> queryCriteria = HttpHelper.buildQueryCriteriaFromRequest(request());
            Map<String,Integer> pagination = HttpHelper.buildPaginationFromRequest(request());
            List<ObjectNode> items = MongoDbHelper.queryMongoDb(Item.getCollection(), queryCriteria,
                    sortCriteria.get(Constants.SORT_FIELD), sortCriteria.get(Constants.SORT_TYPE),
                    pagination.get(Constants.PAGE_NUMBER), pagination.get(Constants.NUM_RESULTS),
                    limitFields, expandFields);
            return ok(HttpHelper.buildDataResponse(200,items));
        } catch (Exception e) {
            Logger.error("Cannot list the items",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Fail to execute the query",e.getMessage()));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result create() {
        JsonNode json = request().body().asJson();
        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
        try {
            if (json.isArray()) {
                List<Item> items = new ArrayList<Item>();
                Map<String,String> createUserIdTable = new HashMap<String,String>();
                for (JsonNode node : json) {
                    if (HttpHelper.isJsonRequestContainValidFields(new Item(),node) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
                    Item newItem = Item.mapJsonToObject(node,null);
                    createUserIdTable.put(newItem.getCreateUserId(),null);
                    items.add(newItem);
                }
                if (createUserIdTable.size() > 1) {
                    return badRequest(HttpHelper.buildErrorFormatResponse(400,"To do bulk create, all the item should have the same create user id"));
                }
                String createUserId = null;
                for (Map.Entry<String,String> entry : createUserIdTable.entrySet()) {
                    createUserId = entry.getKey();
                }
                User createUser = User.getById(createUserId);
                if (createUser == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The create user id " + createUserId + " is invalid"));
                Item.bulkCreate(items);
                return Results.created(HttpHelper.buildDataResponse(201,items));
            } else {
                if (HttpHelper.isJsonRequestContainValidFields(new Item(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
                Item newItem = Item.mapJsonToObject(json,null);
                User createUser = User.getById(newItem.getCreateUserId());
                if (createUser == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The create user id " + newItem.getCreateUserId() + " is invalid"));
                Item.create(newItem);
                return Results.created(HttpHelper.buildDataResponse(201, newItem));
            }

        } catch (Exception e) {
            Logger.error("Create item exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot create item",e.getMessage()));
        }
    }

    public static Result get(String id) {
        Item item = Item.getById(id);
        ObjectNode result = Json.newObject();
        if (item == null) {
            return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the item with id " + id));
        } else {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            if (expandFields != null) item.expandRelatedFields(expandFields);
            if (limitFields != null) {
                item.buildFields();
                Map<String,Object> allFields = item.getFields();
                Map<String,JsonNode> data = GlobalHelper.getLimitFieldsReturn(allFields, limitFields);
                result.putAll(data);
                return ok(HttpHelper.buildDataResponse(200,result));
            }
            return ok(HttpHelper.buildDataResponse(200,item));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result update(String id) {
        JsonNode json = request().body().asJson();
        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
        try {
            Item oldItem = Item.getById(id);
            if (oldItem == null) return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find item with id " + id));
            if (HttpHelper.isJsonRequestContainValidFields(new Item(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
            Item newItem = Item.mapJsonToObject(json, oldItem);
            Item updatedItem = Item.update(newItem);
            if (updatedItem == null) throw new Exception("Fail to update the item. Please try again");
            return ok(HttpHelper.buildDataResponse(200,updatedItem));
        } catch (Exception e) {
            Logger.error("Update item exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot update item",e.getMessage()));
        }
    }

    public static Result delete(String id) {
        try {
            Item item = Item.getById(id);
            if (item == null) {
                return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the item to delete"));
            }
            Item.delete(id);
            return ok(HttpHelper.buildDataResponse(200,"Delete item successfully"));
        } catch (Exception e) {
            Logger.error("Delete item exception", e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot delete item",e.getMessage()));
        }
    }
	
}
