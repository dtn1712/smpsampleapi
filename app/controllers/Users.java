package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import configs.Constants;
import helpers.GlobalHelper;
import helpers.HttpHelper;
import helpers.MongoDbHelper;
import helpers.UserHelper;
import models.AuthToken;
import models.Setting;
import models.User;
import play.Logger;
import play.libs.Json;
import play.mvc.*;
import services.security.SecurityService;

import java.util.List;
import java.util.Map;

public class Users extends Controller{

    public static Result list() {
        try {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            Map<String,String> sortCriteria = HttpHelper.buildSortCriteriaFromRequest(request());
            Map<String,String> queryCriteria = HttpHelper.buildQueryCriteriaFromRequest(request());
            Map<String,Integer> pagination = HttpHelper.buildPaginationFromRequest(request());
            List<ObjectNode> users = MongoDbHelper.queryMongoDb(User.getCollection(), queryCriteria,
                    sortCriteria.get(Constants.SORT_FIELD), sortCriteria.get(Constants.SORT_TYPE),
                    pagination.get(Constants.PAGE_NUMBER), pagination.get(Constants.NUM_RESULTS),
                    limitFields, expandFields);
            return ok(HttpHelper.buildDataResponse(200,users));
        } catch (Exception e) {
            Logger.error("Cannot list the users",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Fail to execute the query",e.getMessage()));
        }
    }


	public static Result get(String id) {
		User user = User.getById(id);
        ObjectNode result = Json.newObject();
		if (user == null) {
			return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find user with id " + id));
		} else {
            boolean isNeedApplySecurity = UserHelper.isNeedApplySecurity(ctx(),id);
            if (isNeedApplySecurity) user.applySecurity();
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            if (limitFields != null) {
                user.buildFields();
                Map<String,Object> allFields = user.getFields();
                Map<String,JsonNode> data = GlobalHelper.getLimitFieldsReturn(allFields, limitFields);
                result.putAll(data);
                return ok(HttpHelper.buildDataResponse(200,result));
            }
            if (isNeedApplySecurity) {
                return ok(HttpHelper.buildDataResponse(200, user.getFields()));
            } else {
                return ok(HttpHelper.buildDataResponse(200, user));
            }
		}
	}

	@BodyParser.Of(BodyParser.Json.class)
	public static Result create() {
		JsonNode json = request().body().asJson();
		if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
		try {
			String facebookId = json.findPath("facebookId").asText();
            String authTokenValue = json.findPath("authToken").asText();
            if (HttpHelper.isJsonRequestContainValidFields(new User(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
            boolean isAuthTokenValid = UserHelper.isAuthTokenValid(authTokenValue);
            if (UserHelper.isUserExist(facebookId) == false) {
                if (isAuthTokenValid) {
                    boolean isFacebookIdValid = UserHelper.isFacebookIdValid(facebookId);
                    if (isFacebookIdValid) {
                        User newUser = User.mapJsonToObject(json,null);
                        User.create(newUser);
                        Setting setting = new Setting();
                        setting.setUserId(newUser.getId());
                        Setting.create(setting);
                        newUser.setSetting(setting);
                        User.update(newUser);
                        AuthToken authToken = new AuthToken();
                        authToken.setFacebookId(facebookId);
                        authToken.setUserId(newUser.getId());
                        authToken.setAuthToken(authTokenValue);
                        AuthToken.create(authToken);
                        return Results.created(HttpHelper.buildDataResponse(201,newUser));
                    } else {
                        return badRequest(HttpHelper.buildErrorFormatResponse(400,"Facebook Id is invalid"));
                    }
                } else {
                    return badRequest(HttpHelper.buildErrorFormatResponse(400,"Cannot create user. Access token is invalid"));
                }
			} else {
                if (isAuthTokenValid) {
                    AuthToken authToken = AuthToken.getOneByUniqueField("facebookId",facebookId);
                    authToken.setAuthToken(authTokenValue);
                    AuthToken.update(authToken);
                    return ok(HttpHelper.buildDataResponse(200,"Found the exist user. Update the token successfully"));
                } else {
                    return ok(HttpHelper.buildDataResponse(200,"User already exists. No need to create"));
                }
			}
		} catch (Exception e) {
			Logger.error("Create user exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot create user",e.getMessage()));
		}
	}

    @With(SecurityService.class)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result update(String id) {
		JsonNode json = request().body().asJson();
		if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
		try {
			User oldUser = User.getById(id);
			if (oldUser == null) return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find user with id " + id));
            if (HttpHelper.isJsonRequestContainValidFields(new User(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400, "The json fields is invalid"));
            User newUser = User.mapJsonToObject(json, oldUser);
			User updatedUser = User.update(newUser);
			if (updatedUser == null) throw new Exception("Fail to update the user. Please try again");
            return ok(HttpHelper.buildDataResponse(200,updatedUser));
		} catch (Exception e) {
			Logger.error("Update user exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot update user",e.getMessage()));
		}
	}

    @With(SecurityService.class)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result follow() {
		JsonNode json = request().body().asJson();
		if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
		try {
			String userLoginId = (String) ctx().args.get(Constants.CURRENT_LOGIN_USER);
            if (userLoginId == null) {
                return unauthorized(HttpHelper.buildErrorFormatResponse(401,"Unauthorized user cannot follow other user"));
            }
			String followUserId = json.findPath("followUserId").asText();
			User userLogin = User.getById(userLoginId);
			User followUser = User.getById(followUserId);
            if (userLogin == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"Current login user id is invalid. Please log out and try again"));
            if (followUser == null) return notFound(HttpHelper.buildErrorFormatResponse(404,"The user you want to follow cannot be found. Please check the user id again"));
            if (followUser.getSetting().isAllowFollowing() == false) return unauthorized(HttpHelper.buildErrorFormatResponse(401,"This user does not allow you to follow him/her"));
			userLogin.addFollowingUser(followUser);
			followUser.addFollowerUser(userLogin);
            User.update(userLogin);
            User.update(followUser);
			return ok(HttpHelper.buildDataResponse(200,"Follow user successfully"));
		} catch (Exception e) {
			Logger.error("Follow user exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot follow user",e.getMessage()));
		}
	}

    @With(SecurityService.class)
    @BodyParser.Of(BodyParser.Json.class)
    public static Result unfollow() {
        JsonNode json = request().body().asJson();
        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
        try {
            String userLoginId = (String) ctx().args.get(Constants.CURRENT_LOGIN_USER);
            if (userLoginId == null) {
                return unauthorized(HttpHelper.buildErrorFormatResponse(401,"Unauthorized user cannot follow other user"));
            }
            String followUserId = json.findPath("unfollowUserId").asText();
            User userLogin = User.getById(userLoginId);
            User unfollowUser = User.getById(followUserId);
            if (userLogin == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"Current login user id is invalid. Please log out and try again"));
            if (unfollowUser == null) return notFound(HttpHelper.buildErrorFormatResponse(404,"The user you want to unfollow cannot be found. Please check the user id again"));
            userLogin.removeFollowingUser(unfollowUser);
            unfollowUser.removeFollowerUser(userLogin);
            User.update(userLogin);
            User.update(unfollowUser);
            return ok(HttpHelper.buildDataResponse(200,"Unfollow user successfully"));
        } catch (Exception e) {
            Logger.error("Unfollow user exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot unfollow user",e.getMessage()));
        }
    }

    @With(SecurityService.class)
    @BodyParser.Of(BodyParser.Json.class)
	public static Result settings() {
		JsonNode json = request().body().asJson();
		if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
		try {
            String userLoginId = (String) ctx().args.get(Constants.CURRENT_LOGIN_USER);
            if (userLoginId == null) {
                return unauthorized(HttpHelper.buildErrorFormatResponse(401,"Unauthorized user cannot change setting"));
            }
			User userLogin = User.getById(userLoginId);
			if (userLogin == null) return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find user"));
            if (HttpHelper.isJsonRequestContainValidFields(new Setting(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400, "The json fields is invalid"));
            Setting setting = Setting.mapJsonToObject(json,userLogin.getSetting());
            setting.setUserId(userLoginId);
			Setting.update(setting);
			userLogin.setSetting(setting);
			User.update(userLogin);
			return ok(HttpHelper.buildDataResponse(200,"Update settings successfully"));
		} catch (Exception e) {
			Logger.error("Update user exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot update setting",e.getMessage()));
		}
	}
	
}
