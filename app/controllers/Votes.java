package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import configs.Constants;
import helpers.GlobalHelper;
import helpers.HttpHelper;
import helpers.MongoDbHelper;
import models.Comparison;
import models.Item;
import models.User;
import models.Vote;
import play.Logger;
import play.libs.Json;
import play.mvc.*;
import services.security.SecurityService;

import java.util.List;
import java.util.Map;

@With(SecurityService.class)
public class Votes extends Controller{

    public static Result list() {
        try {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            Map<String,String> sortCriteria = HttpHelper.buildSortCriteriaFromRequest(request());
            Map<String,String> queryCriteria = HttpHelper.buildQueryCriteriaFromRequest(request());
            Map<String,Integer> pagination = HttpHelper.buildPaginationFromRequest(request());
            List<ObjectNode> votes = MongoDbHelper.queryMongoDb(Vote.getCollection(), queryCriteria,
                    sortCriteria.get(Constants.SORT_FIELD), sortCriteria.get(Constants.SORT_TYPE),
                    pagination.get(Constants.PAGE_NUMBER), pagination.get(Constants.NUM_RESULTS),
                    limitFields, expandFields);
            return ok(HttpHelper.buildDataResponse(200,votes));
        } catch (Exception e) {
            Logger.error("Cannot list the votes",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Fail to execute the query",e.getMessage()));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result create() {
        JsonNode json = request().body().asJson();
        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
        try {
            if (HttpHelper.isJsonRequestContainValidFields(new Vote(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
            Vote newVote = Vote.mapJsonToObject(json,null);
            User createUser = User.getById(newVote.getCreateUserId());
            Item item = Item.getById(newVote.getItemId());
            Comparison comparison = Comparison.getById(newVote.getComparisonId());
            if (createUser == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The create user id " + newVote.getCreateUserId() + " is invalid"));
            if (item == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The item id " + newVote.getItemId() + " is invalid"));
            if (comparison == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The comparison id " + newVote.getComparisonId() + " is invalid"));
            Vote.create(newVote);
            return Results.created(HttpHelper.buildDataResponse(201,newVote));
        } catch (Exception e) {
            Logger.error("Create vote exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot create vote",e.getMessage()));
        }
    }

    public static Result get(String id) {
        Vote vote = Vote.getById(id);
        ObjectNode result = Json.newObject();
        if (vote == null) {
            return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the vote with id " + id));
        } else {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            if (expandFields != null) vote.expandRelatedFields(expandFields);
            if (limitFields != null) {
                vote.buildFields();
                Map<String,Object> allFields = vote.getFields();
                Map<String,JsonNode> data = GlobalHelper.getLimitFieldsReturn(allFields, limitFields);
                result.putAll(data);
                return ok(HttpHelper.buildDataResponse(200,result));
            }
            return ok(HttpHelper.buildDataResponse(200, vote));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result update(String id) {
        JsonNode json = request().body().asJson();
        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
        try {
            Vote oldVote = Vote.getById(id);
            if (oldVote == null) return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find vote with id " + id));
            if (HttpHelper.isJsonRequestContainValidFields(new Vote(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
            Vote newVote = Vote.mapJsonToObject(json, oldVote);
            Vote updatedVote = Vote.update(newVote);
            if (updatedVote == null) throw new Exception("Fail to update the vote. Please try again");
            return ok(HttpHelper.buildDataResponse(200,updatedVote));
        } catch (Exception e) {
            Logger.error("Update vote exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot update vote",e.getMessage()));
        }
    }

    public static Result delete(String id) {
        try {
            Vote vote = Vote.getById(id);
            if (vote == null) {
                return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the vote to delete"));
            }
            Vote.delete(id);
            return ok(HttpHelper.buildDataResponse(200,"Delete vote successfully"));
        } catch (Exception e) {
            Logger.error("Delete vote exception", e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot delete vote",e.getMessage()));
        }
    }

}
