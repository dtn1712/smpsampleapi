package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import configs.Constants;
import helpers.GlobalHelper;
import helpers.HttpHelper;
import helpers.MongoDbHelper;
import models.Comment;
import models.Comparison;
import models.Item;
import models.User;
import play.Logger;
import play.libs.Json;
import play.mvc.*;
import services.security.SecurityService;

import java.util.List;
import java.util.Map;

@With(SecurityService.class)
public class Comments extends Controller{

    public static Result list() {
        try {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            Map<String,String> sortCriteria = HttpHelper.buildSortCriteriaFromRequest(request());
            Map<String,String> queryCriteria = HttpHelper.buildQueryCriteriaFromRequest(request());
            Map<String,Integer> pagination = HttpHelper.buildPaginationFromRequest(request());
            List<ObjectNode> comments = MongoDbHelper.queryMongoDb(Comment.getCollection(),queryCriteria,
                    sortCriteria.get(Constants.SORT_FIELD),sortCriteria.get(Constants.SORT_TYPE),
                    pagination.get(Constants.PAGE_NUMBER),pagination.get(Constants.NUM_RESULTS),
                    limitFields,expandFields);
            return ok(HttpHelper.buildDataResponse(200,comments));
        } catch (Exception e) {
            Logger.error("Cannot list the comments",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Fail to execute the query",e.getMessage()));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result create() {
        JsonNode json = request().body().asJson();
        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
        try {
            if (HttpHelper.isJsonRequestContainValidFields(new Comment(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
            Comment newComment = Comment.mapJsonToObject(json,null);
            User createUser = User.getById(newComment.getCreateUserId());
            Comparison comparison = Comparison.getById(newComment.getComparisonId());
            if (createUser == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The create user id " + newComment.getCreateUserId() + " is invalid"));
            if (comparison == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The comparison id " + newComment.getComparisonId() + " is invalid"));
            Comment.create(newComment);
            return Results.created(HttpHelper.buildDataResponse(201,newComment));
        } catch (Exception e) {
            Logger.error("Create comment exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot create comment",e.getMessage()));
        }
    }

    public static Result get(String id) {
        Comment comment = Comment.getById(id);
        ObjectNode result = Json.newObject();
        if (comment == null) {
            return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the comment with id " + id));
        } else {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            if (expandFields != null) comment.expandRelatedFields(expandFields);
            if (limitFields != null) {
                comment.buildFields();
                Map<String,Object> allFields = comment.getFields();
                Map<String,JsonNode> data = GlobalHelper.getLimitFieldsReturn(allFields,limitFields);
                result.putAll(data);
                return ok(HttpHelper.buildDataResponse(200,result));
            }
            return ok(HttpHelper.buildDataResponse(200,comment));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result update(String id) {
        JsonNode json = request().body().asJson();
        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
        try {
            Comment oldComment = Comment.getById(id);
            if (oldComment == null) return notFound(HttpHelper.buildErrorFormatResponse(404, "Cannot find comment with id " + id));
            if (HttpHelper.isJsonRequestContainValidFields(new Comment(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
            Comment newComment = Comment.mapJsonToObject(json, oldComment);
            Comment updatedComment = Comment.update(newComment);
            if (updatedComment == null) throw new Exception("Fail to update the comment. Please try again");
            return ok(HttpHelper.buildDataResponse(200,updatedComment));
        } catch (Exception e) {
            Logger.error("Update comment exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot update comment",e.getMessage()));
        }
    }

    public static Result delete(String id) {
        try {
            Comment comment = Comment.getById(id);
            if (comment == null) {
                return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the comment to delete"));
            }
            Comment.delete(id);
            return ok(HttpHelper.buildDataResponse(200,"Delete comment successfully"));
        } catch (Exception e) {
            Logger.error("Delete comment exception", e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot delete comment",e.getMessage()));
        }
    }
}
