package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import configs.Constants;
import helpers.GlobalHelper;
import helpers.HttpHelper;
import helpers.MongoDbHelper;
import models.*;
import play.Logger;
import play.libs.Json;
import play.mvc.*;
import services.security.SecurityService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@With(SecurityService.class)
public class Comparisons extends Controller {

    public static Result list() {
        try {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            Map<String,String> sortCriteria = HttpHelper.buildSortCriteriaFromRequest(request());
            Map<String,String> queryCriteria = HttpHelper.buildQueryCriteriaFromRequest(request());
            Map<String,Integer> pagination = HttpHelper.buildPaginationFromRequest(request());
            List<ObjectNode> comparisons = MongoDbHelper.queryMongoDb(Comparison.getCollection(), queryCriteria,
                    sortCriteria.get(Constants.SORT_FIELD), sortCriteria.get(Constants.SORT_TYPE),
                    pagination.get(Constants.PAGE_NUMBER), pagination.get(Constants.NUM_RESULTS),
                    limitFields, expandFields);
            return ok(HttpHelper.buildDataResponse(200,comparisons));
        } catch (Exception e) {
            Logger.error("Cannot list the comparison",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Fail to execute the query",e.getMessage()));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result create() {
        JsonNode json = request().body().asJson();
        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
        try {
            if (HttpHelper.isJsonRequestContainValidFields(new Comparison(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
            Comparison newComparison = Comparison.mapJsonToObject(json,null);
            User createUser = User.getById(newComparison.getCreateUserId());
            Item item1 = Item.getById(newComparison.getItem1Id());
            Item item2 = Item.getById(newComparison.getItem2Id());
            if (createUser == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The create user id " + newComparison.getCreateUserId() + " is invalid"));
            if (item1 == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The item id " + newComparison.getItem1Id() + " is invalid"));
            if (item2 == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The item id " + newComparison.getItem2Id() + " is invalid"));
            Comparison.create(newComparison);
            createUser.addComparisonPost(newComparison);
            item1.setComparisonId(newComparison.getId());
            item2.setComparisonId(newComparison.getId());
            Item.update(item1);
            Item.update(item2);
            User.update(createUser);
            return Results.created(HttpHelper.buildDataResponse(201, newComparison));
        } catch (Exception e) {
            Logger.error("Create comparison exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot create comparison",e.getMessage()));
        }
    }

    public static Result get(String id) {
        Comparison comparison = Comparison.getById(id);
        ObjectNode result = Json.newObject();
        if (comparison == null) {
            return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the comparison with id " + id));
        } else {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            if (expandFields != null) comparison.expandRelatedFields(expandFields);
            if (limitFields != null) {
                comparison.buildFields();
                Map<String,Object> allFields = comparison.getFields();
                Map<String,JsonNode> data = GlobalHelper.getLimitFieldsReturn(allFields, limitFields);
                result.putAll(data);
                return ok(HttpHelper.buildDataResponse(200,result));
            }
            return ok(HttpHelper.buildDataResponse(200, comparison));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result update(String id) {
        JsonNode json = request().body().asJson();
        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
        try {
            Comparison oldComparison = Comparison.getById(id);
            if (oldComparison == null) return notFound(HttpHelper.buildErrorFormatResponse(404, "Cannot find comparison with id " + id));
            if (HttpHelper.isJsonRequestContainValidFields(new Comparison(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
            Comparison newComparison = Comparison.mapJsonToObject(json, oldComparison);
            Comparison updatedComparison = Comparison.update(newComparison);
            if (updatedComparison == null) throw new Exception("Fail to update the comparison. Please try again");
            return ok(HttpHelper.buildDataResponse(200,updatedComparison));
        } catch (Exception e) {
            Logger.error("Update comparison exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot update comparison",e.getMessage()));
        }
    }

    public static Result delete(String id) {
        try {
            Comparison comparison = Comparison.getById(id);
            if (comparison == null) {
                return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the comparison to delete"));
            }
            User createUser = User.getById(comparison.getCreateUserId());
            if (createUser == null) {
                return badRequest(HttpHelper.buildErrorFormatResponse(400,"The create user id " + comparison.getCreateUserId() + " is invalid"));
            }
            createUser.removeComparisonPost(comparison);
            Comparison.delete(id);
            Map<String,String> query = new HashMap<String,String>();
            query.put("comparisonId",id);
            Comment.bulkDelete(query);
            Vote.bulkDelete(query);
            Item.bulkDelete(query);
            ItemAttribute.bulkDelete(query);
            return ok(HttpHelper.buildDataResponse(200,"Delete comparison successfully"));
        } catch (Exception e) {
            Logger.error("Delete comparison exception", e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot delete comparison",e.getMessage()));
        }
    }

    public static Result getComments(String comparisonId) {
        try {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            Map<String,String> sortCriteria = HttpHelper.buildSortCriteriaFromRequest(request());
            Map<String,String> queryCriteria = HttpHelper.buildQueryCriteriaFromRequest(request());
            Map<String,Integer> pagination = HttpHelper.buildPaginationFromRequest(request());
            queryCriteria.put("comparisonId",comparisonId);
            List<ObjectNode> comments = MongoDbHelper.queryMongoDb(Comment.getCollection(), queryCriteria,
                    sortCriteria.get(Constants.SORT_FIELD), sortCriteria.get(Constants.SORT_TYPE),
                    pagination.get(Constants.PAGE_NUMBER), pagination.get(Constants.NUM_RESULTS),
                    limitFields, expandFields);
            return ok(HttpHelper.buildDataResponse(200,comments));
        } catch (Exception e) {
            Logger.error("Cannot list the comparison",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Fail to execute the query",e.getMessage()));
        }
    }

    public static Result getVotes(String comparisonId) {
        try {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            Map<String,String> sortCriteria = HttpHelper.buildSortCriteriaFromRequest(request());
            Map<String,String> queryCriteria = HttpHelper.buildQueryCriteriaFromRequest(request());
            Map<String,Integer> pagination = HttpHelper.buildPaginationFromRequest(request());
            queryCriteria.put("comparisonId",comparisonId);
            List<ObjectNode> votes = MongoDbHelper.queryMongoDb(Vote.getCollection(), queryCriteria,
                    sortCriteria.get(Constants.SORT_FIELD), sortCriteria.get(Constants.SORT_TYPE),
                    pagination.get(Constants.PAGE_NUMBER), pagination.get(Constants.NUM_RESULTS),
                    limitFields, expandFields);
            return ok(HttpHelper.buildDataResponse(200,votes));
        } catch (Exception e) {
            Logger.error("Cannot list the comparison",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Fail to execute the query",e.getMessage()));
        }
    }

    public static Result getItemAttributes(String comparisonId) {
        try {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            Map<String,String> sortCriteria = HttpHelper.buildSortCriteriaFromRequest(request());
            Map<String,String> queryCriteria = HttpHelper.buildQueryCriteriaFromRequest(request());
            Map<String,Integer> pagination = HttpHelper.buildPaginationFromRequest(request());
            queryCriteria.put("comparisonId",comparisonId);
            List<ObjectNode> itemAttributes = MongoDbHelper.queryMongoDb(ItemAttribute.getCollection(), queryCriteria,
                    sortCriteria.get(Constants.SORT_FIELD), sortCriteria.get(Constants.SORT_TYPE),
                    pagination.get(Constants.PAGE_NUMBER), pagination.get(Constants.NUM_RESULTS),
                    limitFields, expandFields);
            return ok(HttpHelper.buildDataResponse(200,itemAttributes));
        } catch (Exception e) {
            Logger.error("Cannot list the comparison",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Fail to execute the query",e.getMessage()));
        }
    }

    public static Result getComment(String comparisonId, String commentId) {
        Comment comment = Comment.getById(commentId);
        ObjectNode result = Json.newObject();
        if (comment == null) {
            return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the comment with id " + commentId));
        } else {
            if (comment.getComparisonId().equals(comparisonId) == false) {
                return badRequest(HttpHelper.buildErrorFormatResponse(400,"The comment id " + commentId + " is not belong to this comparison id " + comparisonId));
            }
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            if (expandFields != null) comment.expandRelatedFields(expandFields);
            if (limitFields != null) {
                comment.buildFields();
                Map<String,Object> allFields = comment.getFields();
                Map<String,JsonNode> data = GlobalHelper.getLimitFieldsReturn(allFields, limitFields);
                result.putAll(data);
                return ok(HttpHelper.buildDataResponse(200,result));
            }
            return ok(HttpHelper.buildDataResponse(200, comment));
        }
    }

    public static Result getVote(String comparisonId, String voteId) {
        Vote vote = Vote.getById(voteId);
        ObjectNode result = Json.newObject();
        if (vote == null) {
            return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the vote with id " + voteId));
        } else {
            if (vote.getComparisonId().equals(comparisonId) == false) {
                return badRequest(HttpHelper.buildErrorFormatResponse(400,"The vote id " + voteId + " is not belong to this comparison id " + comparisonId));
            }
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            if (expandFields != null) vote.expandRelatedFields(expandFields);
            if (limitFields != null) {
                vote.buildFields();
                Map<String,Object> allFields = vote.getFields();
                Map<String,JsonNode> data = GlobalHelper.getLimitFieldsReturn(allFields, limitFields);
                result.putAll(data);
                return ok(HttpHelper.buildDataResponse(200,result));
            }
            return ok(HttpHelper.buildDataResponse(200, vote));
        }
    }

    public static Result getItemAttribute(String comparisonId, String itemAttributeId) {
        ItemAttribute itemAttribute = ItemAttribute.getById(itemAttributeId);
        ObjectNode result = Json.newObject();
        if (itemAttribute == null) {
            return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the item attribute with id " + itemAttributeId));
        } else {
            if (itemAttribute.getComparisonId().equals(comparisonId) == false) {
                return badRequest(HttpHelper.buildErrorFormatResponse(400,"The itemAttribute id " + itemAttributeId + " is not belong to this comparison id " + comparisonId));
            }
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            if (expandFields != null) itemAttribute.expandRelatedFields(expandFields);
            if (limitFields != null) {
                itemAttribute.buildFields();
                Map<String,Object> allFields = itemAttribute.getFields();
                Map<String,JsonNode> data = GlobalHelper.getLimitFieldsReturn(allFields, limitFields);
                result.putAll(data);
                return ok(HttpHelper.buildDataResponse(200,result));
            }
            return ok(HttpHelper.buildDataResponse(200, itemAttribute));
        }
    }

}
