package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import configs.Constants;
import helpers.GlobalHelper;
import helpers.HttpHelper;
import helpers.MongoDbHelper;
import models.Comparison;
import models.ItemAttribute;
import play.Logger;
import play.libs.Json;
import play.mvc.*;
import services.security.SecurityService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dtn29 on 4/22/14.
 */
@With(SecurityService.class)
public class ItemAttributes extends Controller {

    public static Result list() {
        try {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            Map<String,String> sortCriteria = HttpHelper.buildSortCriteriaFromRequest(request());
            Map<String,String> queryCriteria = HttpHelper.buildQueryCriteriaFromRequest(request());
            Map<String,Integer> pagination = HttpHelper.buildPaginationFromRequest(request());
            List<ObjectNode> itemAttributes = MongoDbHelper.queryMongoDb(ItemAttribute.getCollection(), queryCriteria,
                    sortCriteria.get(Constants.SORT_FIELD), sortCriteria.get(Constants.SORT_TYPE),
                    pagination.get(Constants.PAGE_NUMBER), pagination.get(Constants.NUM_RESULTS),
                    limitFields, expandFields);
            return ok(HttpHelper.buildDataResponse(200,itemAttributes));
        } catch (Exception e) {
            Logger.error("Cannot list the item attributes", e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Fail to execute the query",e.getMessage()));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result create() {
        JsonNode json = request().body().asJson();
        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
        try {
            if (json.isArray()) {
                List<ItemAttribute> itemAttributes = new ArrayList<ItemAttribute>();
                Map<String,String> comparisonIdTable = new HashMap<String,String>();
                for (JsonNode node : json) {
                    if (HttpHelper.isJsonRequestContainValidFields(new ItemAttribute(),node) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
                    ItemAttribute newItemAttribute = ItemAttribute.mapJsonToObject(node,null);
                    comparisonIdTable.put(newItemAttribute.getComparisonId(),null);
                    itemAttributes.add(newItemAttribute);
                }
                if (comparisonIdTable.size() > 1) {
                    return badRequest(HttpHelper.buildErrorFormatResponse(400,"To do bulk create, all the item attribute should have the same comparison id"));
                }
                String comparisonId = null;
                for (Map.Entry<String,String> entry : comparisonIdTable.entrySet()) {
                    comparisonId = entry.getKey();
                }
                Comparison comparison = Comparison.getById(comparisonId);
                if (comparison == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The comparison id " + comparisonId + " is invalid"));
                ItemAttribute.bulkCreate(itemAttributes);
                return Results.created(HttpHelper.buildDataResponse(201,itemAttributes));
            } else {
                if (HttpHelper.isJsonRequestContainValidFields(new ItemAttribute(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
                ItemAttribute newItemAttribute = ItemAttribute.mapJsonToObject(json,null);
                Comparison comparison = Comparison.getById(newItemAttribute.getComparisonId());
                if (comparison == null) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The comparison id " + newItemAttribute.getComparisonId() + " is invalid"));
                ItemAttribute.create(newItemAttribute);
                return Results.created(HttpHelper.buildDataResponse(201, newItemAttribute));
            }
        } catch (Exception e) {
            Logger.error("Create item attribute exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot create item attribute",e.getMessage()));
        }
    }

    public static Result get(String id) {
        ItemAttribute itemAttribute = ItemAttribute.getById(id);
        ObjectNode result = Json.newObject();
        if (itemAttribute == null) {
            return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the item attribute with id " + id));
        } else {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            if (expandFields != null) itemAttribute.expandRelatedFields(expandFields);
            if (limitFields != null) {
                itemAttribute.buildFields();
                Map<String,Object> allFields = itemAttribute.getFields();
                Map<String,JsonNode> data = GlobalHelper.getLimitFieldsReturn(allFields, limitFields);
                result.putAll(data);
                return ok(HttpHelper.buildDataResponse(200,result));
            }
            return ok(HttpHelper.buildDataResponse(200,itemAttribute));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result update(String id) {
        JsonNode json = request().body().asJson();
        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
        try {
            ItemAttribute oldItemAttribute = ItemAttribute.getById(id);
            if (oldItemAttribute == null) return notFound(HttpHelper.buildErrorFormatResponse(404, "Cannot find item attribute with id " + id));
            if (HttpHelper.isJsonRequestContainValidFields(new ItemAttribute(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
            ItemAttribute newItemAttribute = ItemAttribute.mapJsonToObject(json, oldItemAttribute);
            ItemAttribute updatedItemAttribute = ItemAttribute.update(newItemAttribute);
            if (updatedItemAttribute == null) throw new Exception("Fail to update the item attribute. Please try again");
            return ok(HttpHelper.buildDataResponse(200,updatedItemAttribute));
        } catch (Exception e) {
            Logger.error("Update item attribute exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot update item attribute",e.getMessage()));
        }
    }

    public static Result delete(String id) {
        try {
            ItemAttribute itemAttribute = ItemAttribute.getById(id);
            if (itemAttribute == null) {
                return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the item attribute to delete"));
            }
            ItemAttribute.delete(id);
            return ok(HttpHelper.buildDataResponse(200,"Delete item attribute successfully"));
        } catch (Exception e) {
            Logger.error("Delete item attribute exception", e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot delete item attribute",e.getMessage()));
        }
    }

}
